// CREATING AN API SERVER USING EXPRESS

/*
	Identify the proper component/ingredient to start the project.

	Use the 'require()' directive to load the express module/package.
	express --> this will allowus to access emthods and functions that will crate a server easier.

*/
const express = require('express');

/*
	Create an application using express.
	express() --> creates an express/instant application and we will give an identifier for the app that it will produce.
	In layman's term, the application will be our server.
*/

const application = express();

	// Identify a virtual port in which to serve the project.
const port = 4000;

	// Assign the establsihed connection/server into the designated port.
application.listen(port, () => console.log(`Server is running on ${port}`));

//NodeJS Counterpart
/*
	const http = require('http');

	const port = 4000;

	http.createServer((req, res) => {
		//statement
		res.write('Hello');
		res.end();
	}).listen(port);

	console.log('The server is running');
*/